<?php

include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
 
sec_session_start();
?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Venue View | Search for Venues</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
   
    <link href="css/jumbotron-narrow.css" rel="stylesheet">

    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="index.php">Home</a></li>
			<?php
			if (login_check($mysqli) == true) {
                echo '<li role="presentation" class="default"><a href="index.php">Account</a></li>';
            } else {
                echo '<li role="presentation"><a href="login.php">Log in</a></li>';
}
			?>
          </ul>
        </nav>
        <h3 class="text-muted">Venue View</h3>
      </div>

      <div class="row marketing">
        <div class="col-lg-12">
          
		    <form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
		      <div class="row">
                <div class="col-lg-12">
                  <div class="input-group input-group-lg">
                    <input type="text" class="form-control input-lg" name="search" placeholder="Search for venue name, city, or state">
                    <span class="input-group-btn">
                      <button class="btn btn-default btn-lg" type="submit">Search</button>
                    </span>
                  </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
              </div><!-- /.row -->
	        </form>		 		
            <br />
			<p>     or browse venues <a href="select_cities.php">here</a>.</p>
        </div>
      </div>
	  
	  <?php
        if($_POST){
		    //executed if search is submitted
			
			//create checkbox form for results
            echo '<form action="protected_page.php" method="get">';
		    //Get search variables from post array
	        $key = mysql_real_escape_string($_POST['search']);
		    $count = 0;
		    $currentCity = 'null';
		    $currentZip = 0;
		    $currentState = 'null';
		
		    //search by venue name
		    $nameQuery = "SELECT * FROM venues WHERE v_name LIKE '%$key%'";
		    $nameResult = $mysqli->query($nameQuery) or die($mysqli->error.__LINE__);
		
			//search by city
			$cityQuery = "SELECT * FROM cities 
						  INNER JOIN venues ON venues.v_city=cities.zip_code 
						  WHERE cities.city_name LIKE '%$key%'
						  ORDER BY cities.city_name";
			$cityResult = $mysqli->query($cityQuery) or die($mysqli->error.__LINE__);
			
			//search by zipcode
			$zipQuery = "SELECT * FROM cities 
						 INNER JOIN venues ON venues.v_city=cities.zip_code 
						 WHERE cities.zip_code LIKE '%$key%'";
			$zipResult = $mysqli->query($zipQuery) or die($mysqli->error.__LINE__);
			
			
			//search by state
			$stateQuery = "SELECT * FROM cities 
						   INNER JOIN venues ON venues.v_city=cities.zip_code 
						   WHERE cities.state LIKE '%$key%'";
			$stateResult = $mysqli->query($stateQuery) or die($mysqli->error.__LINE__);
		
		    //search by venue name results
			if($nameResult->num_rows > 0){
			  $count++;
			  echo 'Venue name matches:';
			  echo '<br />';
			  while($row = $nameResult->fetch_assoc()){
				echo '<input type="checkbox" name="venueNames[]" value="'.$row['v_id'].'" />'.$row['v_name'].'<br />';   
			  }
			  echo '<br />';
			}
		
			//search by city results
			if($cityResult->num_rows > 0){
				$count++;
				echo "City matches:";
				echo '<br />';
				while($row = $cityResult->fetch_assoc()){
					if (!($row['city_name']==$currentCity)){
						echo '<br />';
						echo 'Venues in '.$row['city_name'].'<br />';
					}
					echo '<input type="checkbox" name="venueNames[]" value="'.$row['v_id'].'" />'.$row['v_name'].'<br />';
					
					$currentCity=$row['city_name'];
				}
				echo '<br />';
			}
			
			//search by zip results
			if($zipResult->num_rows > 0){
				$count++;
				echo "Zip Code matches";
				echo '<br />';
				
				while($row = $zipResult->fetch_assoc()){
					if (!($row['zip_code']==$currentZip)){
						echo '<br />';
						echo $row['zip_code'].'<br />';
					}
					echo '<input type="checkbox" name="venueNames[]" value="'.$row['v_id'].'" />'.$row['v_name'].'<br />';
					
					$currentZip=$row['zip_code'];
				}
			}
		
			//search by state results
			if($stateResult->num_rows > 0){
				$count++;
				echo "state matches:";
				echo '<br />';
				while($row = $stateResult->fetch_assoc()){
					if (!($row['state']==$currentState)){
						echo '<br />';
						echo $row['state'].'<br />';
					}
					echo '<input type="checkbox" name="venueNames[]" value="'.$row['v_id'].'" />'.$row['v_name'].'<br />';
					echo '<br />';
					
					$currentState=$row['state'];
				}
				echo '<br />';
			}
		
			if($count==0){
				echo 'no matches';
			}else{
				echo '<input type="submit" name="formSubmit" value="Submit" />
				</form>';
			}
	    }
    ?>	

      <footer class="footer">
        <p>&copy; Company 2014</p>
      </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
