
<?php
include_once 'includes/db_connect.php';
    //Create the select query
	$query = "SELECT DISTINCT city_name
              FROM cities";
    //get results
	$result = $mysqli->query($query) or die($mysqli->error.__LINE__);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Venue View | Home</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
   
    <link href="css/jumbotron-narrow.css" rel="stylesheet">

    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="index.php">Home</a></li>
            <li role="presentation"><a href="login.php">Log in</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">Venue View</h3>
      </div>

      
<form action="select_venues.php" method="post">
	<label for='formCities[]'>Select the cities your favorite venues are in:</label><br>
	<select multiple="multiple" class="form-control" name="formCities[]">
		<?php
			//Check if at least one row is found
			if($result->num_rows > 0){
				//loop through results
				//while there's still some rows left
				while($row = $result->fetch_assoc()){
					//display customer info
					$output = '<option value="'.$row['city_name'].'">'.$row['city_name'].'</option>';
					
					//echo output
					echo $output;
				}
			} else {
				echo "Sorry, no cities were found";
			}
			?>
	</select><br>
	<input type="submit" class="btn btn-default" name="formSubmit" value="Submit" >
</form>
        <br />
      

      <footer class="footer">
        <p>&copy; Company 2014</p>
      </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
