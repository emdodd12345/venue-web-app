<?php
include_once 'db_connect.php';
include_once 'includes/functions.php';
include_once 'psl-config.php';

sec_session_start();
 
$id = htmlentities($_SESSION['user_id']);
 
$error_msg = "";
 
if (isset($_POST['p'], $_POST['np'])) {
    // Sanitize and validate the data passed in
   
 
    $password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
	$newPassword = filter_input(INPUT_POST, 'np', FILTER_SANITIZE_STRING);
    if (strlen($newPassword) != 128) {
        // The hashed pwd should be 128 characters long.
        // If it's not, something really odd has happened
        $error_msg .= '<p class="error">Invalid password configuration.</p>';
    }
 
    // password validity have been checked client side.
    // This should should be adequate as nobody gains any advantage from
    // breaking these rules.
    //
 
     if ($stmt = $mysqli->prepare("SELECT password, salt
        FROM members
        WHERE id = ?
        LIMIT 1")) {
        $stmt->bind_param('i', $id);  // Bind "$email" to parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($db_password, $salt);
        $stmt->fetch();
 
        // hash the password with the unique salt.
        $password = hash('sha512', $password . $salt);
        if ($stmt->num_rows == 1) {
            // If the user exists 
 
            
                // Check if the password in the database matches
                // the password the user submitted.
                if ($db_password != $password) {
                
                    // Password is not correct
                    // We record this attempt in the database
                    $now = time();
                    $mysqli->query("INSERT INTO login_attempts(user_id, time)
                                    VALUES ('$id', '$now')");
                    $error_msg .= '<p class="error">Invalid password.</p>';
                }
            
        } else {
            // No user exists.
            $error_msg .= '<p class="error">Invalid user.</p>';
        }
    }

    // TODO: 
    // We'll also have to account for the situation where the user doesn't have
    // rights to do registration, by checking what type of user is attempting to
    // perform the operation.
 
    if (empty($error_msg)) {
        // Create a random salt
        //$random_salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE)); // Did not work
        $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
 
        // Create salted password 
        $newPassword = hash('sha512', $newPassword . $random_salt);
 
        // Insert the new user into the database 
        if ($insert_stmt = $mysqli->prepare("UPDATE members SET password = ?, salt = ? WHERE id=?")) {
            $insert_stmt->bind_param('ssi', $newPassword, $random_salt, $id);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                header('Location: ../error.php?err=Registration failure: INSERT');
            }
        }
		$_SESSION['login_string'] = hash('sha512', 
                              $newPassword . $_SERVER['HTTP_USER_AGENT']);
		$msg="Password Updated";
		header('Location: ./account.php?msg='.urlencode($msg).'');
        
    }
}