<?php 
function editPassword($oldPassword, $newPassword, $confirmNewPassword $mysqli) {
    // Using prepared statements means that SQL injection is not possible. 
	$id = htmlentities($_SESSION['user_id']);
    if ($stmt = $mysqli->prepare("SELECT password, salt
        FROM members
        WHERE id = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $id);  // Bind "$id" to parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($db_password, $salt);
        $stmt->fetch();
 
        // hash the password with the unique salt.
        $oldPassword = hash('sha512', $oldPassword . $salt);
        if ($stmt->num_rows == 1) {
            // If the user exists we check if the account is locked
            // from too many login attempts 
 
            /*if (checkbrute($user_id, $mysqli) == true) {
                // Account is locked 
                // Send an email to user saying their account is locked
                return false;
            } else {*/
                // Check if the password in the database matches
                // the password the user submitted.
                if ($db_password == $password) {
                    // Password is correct!
                    // Get the user-agent string of the user.
                    
                    // Login successful.
                    return true;
                } else {
                    // Password is not correct
                    // We record this attempt in the database
                    /*$now = time();
                    $mysqli->query("INSERT INTO login_attempts(user_id, time)
                                    VALUES ('$user_id', '$now')");*/
                    return false;
                }
            
        } else {
            // No user exists.
            return false;
        }
    }
}
?>