
<?php
include_once 'includes/changePassword.inc.php';
include_once 'includes/functions.php';
 
if (login_check($mysqli) == true) {
    $id = htmlentities($_SESSION['user_id']);
    
} else {
    header('Location: index.php');	
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Venue View | Change Password</title>
<script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
   
    <link href="css/jumbotron-narrow.css" rel="stylesheet">

    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="index.php">Home</a></li>
			<li role="presentation" class="default"><a href="account.php">Account</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">Venue View</h3>
      </div>
	  
    <h2>Change Password</h2>
      <?php
        if (!empty($error_msg)) {
            echo $error_msg;
        }
        ?>
      <ul>
    
            <li>Passwords must be at least 6 characters long</li>
            <li>Passwords must contain
                <ul>
                    <li>At least one uppercase letter (A..Z)</li>
                    <li>At least one lower case letter (a..z)</li>
                    <li>At least one number (0..9)</li>
                </ul>
            </li>
            <li>Your password and confirmation must match exactly</li>
        </ul>
        <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" 
                method="post" 
                name="change_Password_form">
            Password: <input type="password"
                             name="password" 
                             id="password"/><br>
            New Password: <input type="password"
                             name="newPassword" 
                             id="newPassword"/><br>
            Confirm password: <input type="password" 
                                     name="confirmpwd" 
                                     id="confirmpwd" /><br>
            <input type="button" 
                   value="Submit" 
                   onclick="return changepwdformhash(this.form,
                                   this.form.password,
                                   this.form.newPassword,
                                   this.form.confirmpwd);" /> 
        </form>
        

      <footer class="footer">
        <p>&copy; Company 2014</p>
      </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
