<?php include('includes/db_connect.php');?>
<?php
    //Create the select query
	$query = "SELECT DISTINCT city_name
              FROM cities";
    //get results
	$result = $mysqli->query($query) or die($mysqli->error.__LINE__);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Venue View | Select Favorite Venues</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
   
    <link href="css/jumbotron-narrow.css" rel="stylesheet">

    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="index.php">Home</a></li>
            <li role="presentation"><a href="login.php">Log in</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">Venue View</h3>
      </div>

      
<h1>Select your favorite venues:</h1>
<form action="protected_page.php" method="get">
<?php
if(isset($_POST['formCities'])) {
			      
$name = $_POST['formCities'];

foreach ($name as $city){
    echo '<p>';
    echo $city.'<br />';
    $query = "SELECT venues.v_id, venues.v_name FROM venues
              INNER JOIN cities
              ON cities.zip_code=venues.v_city
              WHERE cities.city_name='$city'";
    $result = $mysqli->query($query) or die($mysqli->error.__LINE__);

    if($result->num_rows > 0){
				
	while($row = $result->fetch_assoc()){
		
		echo '<input type="checkbox" name="venueNames[]" value="'.$row['v_id'].'" />'.$row['v_name'].'<br />';
					
	}
    } else {
	echo "Sorry, no venues in ".$city." were found";
    }
echo '</p>';
    
}
} else{
	echo "you did not select any cities";
}

if(isset($_POST['formCities'])) {

echo '<input type="submit" name="formSubmit" value="Submit" />
</form>';}
 ?>     
<br />
      <footer class="footer">
        <p>&copy; Company 2014</p>
      </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>