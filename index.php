<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
 
sec_session_start();
 
if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Venue View | Home</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
   
    <link href="css/jumbotron-narrow.css" rel="stylesheet">

    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="index.php">Home</a></li>
            
			<?php
			if (login_check($mysqli) == true) {
                echo '<li role="presentation" class="default"><a href="account.php">Account</a></li>';
            } else {
                echo '<li role="presentation"><a href="login.php">Log in</a></li>';
}
			?>
          </ul>
        </nav>
        <h3 class="text-muted">Venue View</h3>
      </div>

      <?php
        if (login_check($mysqli) == true) {
            echo '<p>Currently logged ' . $logged . ' as ' . htmlentities($_SESSION['username']) . '.</p>';
			
			echo '<p>View your favorites list <a href="protected_page.php">here</a>.</p>';
						
 
            echo '<p>Do you want to change user? <a href="includes/logout.php">Log out</a>.</p>';
        } else {
            echo '<p>Currently logged ' . $logged . '.</p>';
			echo "<p>If you're already a member <a href='login.php'>log in</a> here</p>";
            echo "<p>If you don't have an account, please <a href='register.php'>register</a></p>";
        }
      ?>      
      <br />
     
      <footer class="footer">
        <p>&copy; Company 2014</p>
      </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
