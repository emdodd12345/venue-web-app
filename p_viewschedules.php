
<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
require 'includes/eventfulDatabaseAccess.php'; 
sec_session_start();
 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Venue View | Home</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
   
    <link href="css/jumbotron-narrow.css" rel="stylesheet">

    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="index.php">Home</a></li>
            <?php
			if (login_check($mysqli) == true) {
                echo '<li role="presentation" class="default"><a href="account.php">Account</a></li>';
            } else {
                echo '<li role="presentation"><a href="login.php">Log in</a></li>';
}
			?>
          </ul>
        </nav>
        <h3 class="text-muted">Venue View</h3>
      </div>

      <?php if (login_check($mysqli) == true) : ?>
           
		<p>
        <?php
        $id = htmlentities($_SESSION['user_id']);				
		$query = "SELECT venue_id FROM member_favorites
                  WHERE member_id='$id'";
        $result = $mysqli->query($query) or die($mysqli->error.__LINE__);

        if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
	            $vid = $row['venue_id'];
				$query1 = "SELECT v_name FROM venues
                           WHERE v_id = '$vid'";
                $result1 = $mysqli->query($query1) or die($mysqli->error.__LINE__);

                while($row1 = $result1->fetch_assoc()){
					echo '<br />';
					echo '<h2>'.$row1['v_name'].'</h2>';
				}
	        
				getSchedule($vid);
					
	        }
        } else {
	        echo "You haven't chosen any favorite venues!";
			echo '<p><a href="search.php">Add more venues</a></p>';
        }
	    ?>
		
        </p>
        <p>Return to <a href="index.php">login page</a></p>
		
      <?php else : ?>
        <p>
          <span class="error">You are not authorized to access this page.</span> Please <a href="index.php">login</a>.
        </p>
      <?php endif; ?>

      <br />
      

      <footer class="footer">
        <p>&copy; Company 2014</p>
      </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
