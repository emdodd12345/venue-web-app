
<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
 
sec_session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Venue View | Your List</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
   
    <link href="css/jumbotron-narrow.css" rel="stylesheet">

    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="index.php">Home</a></li>
			<li role="presentation" class="default"><a href="account.php">Account</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">Venue View</h3>
      </div>
    <?php 
		if (login_check($mysqli) == true) : ?>
            <p>Welcome <?php echo htmlentities($_SESSION['username']); ?>!</p>
            <p>
			<?php 
			  $id = htmlentities($_SESSION['user_id']);
			  if(isset($_GET['venueNames'])) {
			      $venueNames = $_GET['venueNames'];
                    foreach($venueNames as $venue){
				        $query1 = "INSERT IGNORE INTO `member_favorites`(`venue_id`, `member_id`) 
                                   VALUES ('$venue', $id)";
				   
		                $result1 = $mysqli->query($query1) or die($mysqli->error.__LINE__);
			        }  
					header('Location: protected_page.php');
              }
			  
			  
			  if(isset($_REQUEST['removeVenue'])) {
			      $removeVenue = $_REQUEST['removeVenue'];
                    foreach($removeVenue as $venue){
						echo $venue;
				        $queryRemove = "DELETE FROM member_favorites 
                                        WHERE venue_id='$venue' AND member_id=$id;";
				   
		                $resultRemove = $mysqli->query($queryRemove) or die($mysqli->error.__LINE__);
			        }
                    header('Location: protected_page.php');					
              }
		
			  $query = "SELECT venue_id FROM member_favorites
              WHERE member_id='$id'";
              $result = $mysqli->query($query) or die($mysqli->error.__LINE__);

              if($result->num_rows > 0){
				
	              while($row = $result->fetch_assoc()){
	                  $vid = $row['venue_id'];
					  
					  $query2 = "SELECT * FROM venues
                                 WHERE v_id='$vid'";
                      $result2 = $mysqli->query($query2) or die($mysqli->error.__LINE__);

                      if($result2->num_rows > 0){
				
	                      while($row2 = $result2->fetch_assoc()){
	                          echo $row2['v_name'].'<br />';
	                      }
                      } else {
	                      echo "uh oh. You haven't chosen any favorite venues!";
						  
                      }
					
	              }
             } else {
	              echo "You haven't chosen any favorite venues!";
             }
			  ?>
            </p>
			<p><a href="search.php">Add more venues</a></p>
			<p><a href="remove_venues.php">Remove venues</a></p>
			<p><a href="p_viewschedules.php">View Schedules</a></p>
            <p>Do you want to change user? <a href="includes/logout.php">Log out</a></p>
        <?php else : ?>
            <p>
                <span class="error">You are not authorized to access this page.</span> Please <a href="login.php">login</a>.
            </p>
        <?php endif; ?>
      

      <br />
      

      <footer class="footer">
        <p>&copy; Company 2014</p>
      </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
