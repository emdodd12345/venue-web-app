Venue View Version 1.0 07/25/2015

GENERAL DESCRIPTION
--------------------

- This web app allows users to easily view the event schedules of their favorite venues.

- Users can browse venues by city or search for venues based on venue name, city, or state. 

- Users can create an account can save a list of their favorite venues so they can easily access the schedules of those venues when logging in.