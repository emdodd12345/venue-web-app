<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
 
sec_session_start();
if (login_check($mysqli) == true) {
    $id = htmlentities($_SESSION['user_id']);
    
} else {
    header('Location: index.php');	
}




$accInfoQuery = "SELECT * FROM members WHERE id = '$id'";
$accInfoResult = $mysqli->query($accInfoQuery) or die($mysqli->error.__LINE__);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Venue View | Account</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
   
    <link href="css/jumbotron-narrow.css" rel="stylesheet">

    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="index.php">Home</a></li>
			<li role="presentation" class="default"><a href="account.php">Account</a></li>
            
          </ul>
        </nav>
        <h3 class="text-muted">Venue View</h3>
      </div>

      
		<?php if(isset($_GET['msg'])){
			echo '<div class="msg">'.$_GET['msg'].'</div>';
		}?>
          <h2>Account Info</h2>
		  
			<?php
			//Check if at least one row is found
			if($accInfoResult->num_rows > 0){
				//loop through results
				//while there's still some rows left
				while($row = $accInfoResult->fetch_assoc()){
					//display customer info
					
					$output = '<p>'.'<h4>'.'Username '.'</h4>'.$row['username'].'</p>';
					//$output.= '<p><a href="edit.php" class="btn btn-default btn-sm">Edit</a></p>';
					$output.= '<p>'.'<h4>'.'Email '.'</h4>'.$row['email'].'</p>';
					//$output.= '<p><a href="editEmail.php" class="btn btn-default btn-sm">Edit</a></p>';
					$output.= '<p>'.'<h4>'.'Password '.'</h4>';
					$output.= '<p>'.'<a href="editPassword.php" class="btn btn-default btn-sm">Change password</a></p>'.'</p>';
					
					
					//echo output
					echo $output;
				}
			} else {
				echo "Sorry, no customers were found";
			}
			?>
          </table>

     <footer class="footer">
        <p>&copy; Company 2014</p>
      </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
